FROM nanguide/alpinego

COPY *.go $GOPATH/src/offerspkg/

COPY ./offersmain/*.go $GOPATH/src/offers/

RUN go install offerspkg

RUN go install offers

CMD ["/go/bin/offers"]
