package main

import (
    "github.com/gorilla/mux"
    "encoding/json"
    "net/http"
    "offerspkg"
)

func mainHandler(w http.ResponseWriter, r *http.Request) {
    offers := offerspkg.Offers{
        offerspkg.Offer{Excursion: "Прогулочная по Москве", Gid: "Александр", Language: "Китайский", Price: "320432"},
        offerspkg.Offer{Excursion: "Высоцкий", Gid: "Елена", Language: "Русский", Price: "3432"},
    }
    json.NewEncoder(w).Encode(offers)
}

func main() {
    Router := mux.NewRouter().StrictSlash(true)
    Router.HandleFunc("/", mainHandler).Methods("GET")
    http.ListenAndServe(":8080", Router)
}
