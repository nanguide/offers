package offerspkg

type Offer struct {
    Excursion string `json: "excursion"`
    Gid       string `json: "gid"`
    Language  string `json: "language"`
    Price     string `json: "price"`
}

type Offers []Offer
